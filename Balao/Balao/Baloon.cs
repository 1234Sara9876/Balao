﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balao
{
    /// <summary>
    /// Classe que representa balões cuja distância ao solo e direcção podem ser conhecidas a cada momento
    /// </summary>
    public class Baloon
    {
        #region atributos

        private string cores;
        private string direçao;
        private double altura;

        #endregion

        #region propriedades

        public string Cores { get; set; }
        public string Direçao{ get; set; }
        public double Altura{ get; set; }

        #endregion

        #region construtores

        public Baloon()
        {
            Cores = string.Empty;
            Direçao = string.Empty;
            Altura = 0.0;
        }  //construtor default

        public Baloon(string cores, string direçao,double altura)
        {
            Cores = cores;
            Direçao = direçao;
            Altura = altura;
        }//construtor por parâmetros

        public Baloon(Baloon b):this(b.Cores, b.Direçao, b.Altura) { }
        //construtor de cópia

        #endregion

        #region Métodos 

        public void Subir (double x)
        {
            if (Altura >= 0)
            {
                Altura += x;
            }
        }

        public void Descer (double y)
        {
            if (Altura-y>=0)
            {
                Altura -= y;
            }
            else
            {
                Altura = 0;
            }
        }
        #endregion


    }
}
